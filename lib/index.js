'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.$resource = exports.$http = undefined;

var _superagent = require('superagent');

var _superagent2 = _interopRequireDefault(_superagent);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const getToken = token => {
  if (typeof window !== 'undefined') {
    return localStorage.getItem('token');
  }
  return token;
};

const prepareData = data => {
  if ((0, _lodash.isArray)(data)) {
    return data;
  }
  const result = {};
  Object.keys(data).forEach(el => {
    if ((0, _lodash.isNull)(data[el]) || (0, _lodash.isUndefined)(data[el])) {
      return;
    }
    if ((0, _lodash.isObject)(data[el])) {
      return result[el] = prepareData(data[el]);
    }
    result[el] = data[el];
  });
  return result;
};

const getReq = (url, query, token = null) => {
  return new Promise((resolve, reject) => {
    _superagent2.default.get(url).set('Content-Type', 'application/json').set('Authorization', getToken(token)).query(query).end((err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

const postReq = (url, data, token = null) => {
  return new Promise((resolve, reject) => {
    _superagent2.default.post(url).send(prepareData(data)).set('Content-Type', 'application/json').set('Authorization', getToken(token)).end((err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

const postReqUpload = (url, data, token = null) => {
  return new Promise((resolve, reject) => {
    _superagent2.default.post(url).set('Authorization', getToken(token)).send(data).end((err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

const putReq = (url, data, token = null) => {
  return new Promise((resolve, reject) => {
    _superagent2.default.put(url).send(prepareData(data)).set('Content-Type', 'application/json').set('Authorization', getToken(token)).end((err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

const deleteReq = (url, token = null) => {
  return new Promise((resolve, reject) => {
    _superagent2.default.delete(url).set('Content-Type', 'application/json').set('Authorization', getToken(token)).end((err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};

const $http = exports.$http = options => {
  return {
    post(url, data, opts) {
      return postReq(`${options.root}/${url}`, data, opts.token);
    },
    get(url, query, opts) {
      return getReq(`${options.root}/${url}`, query, opts.token);
    },
    put(url, data, opts) {
      return putReq(`${options.root}/${url}`, data, opts.token);
    },
    delete(url, opts) {
      return deleteReq(`${options.root}/${url}`, opts.token);
    },
    postUpload(url, data, opts) {
      return postReqUpload(`${options.root}/${url}`, data, opts.token);
    }
  };
};

const $resource = exports.$resource = options => {
  return url => {
    return {
      query(query) {
        return getReq(`${options.root}/${url}`, query, options.token);
      },
      get(opts) {
        return getReq(`${options.root}/${url}/${opts.id}`, opts.params, options.token);
      },
      create(data) {
        return postReq(`${options.root}/${url}`, data, options.token);
      },
      update(data, opts) {
        return putReq(`${options.root}/${url}/${data.id || opts.id}`, data, options.token);
      },
      delete(opts) {
        return deleteReq(`${options.root}/${url}/${opts.id}`, options.token);
      }
    };
  };
};