# Intro

This is libray for my personal use and should be used with [nihapi](https://gitlab.com/Nimdis/nihapi)

# Install

Add the following lines to your package.json

```
"resource": "https://gitlab.com/Nimdis/resource.git"
```

And run ```npm install``` or ```yarn install```

# Usage

The library separated to two parts. Usage of each one depends on your case. If you are trying get data from restful resource just use $resource else $http. Easy.

## $resource

* Import the library

```
import { $resource } from 'resource'
```

* Initializing

```
const resource = $resource({
  root: 'localhost:3000/api/v1',
  token: '751300338e065b2d4e688a1230f183f7'
})
```

* Query records

```
resource('exchanges')
  .query({ page_size: 100 })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Get one record

```
resource('exchanges')
  .get({ id: 1, params: { /* some params */ } })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Create a record

```
resource('exchanges')
  .create({ id: 1, name: 'first record' })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Update the record

```
resource('exchanges')
  .update({ 
    id: 1, 
    name: 'first record' 
    }, { 
      id: 1 /* or leave this attribute empty if first one has an id field */ 
  })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Delete the record

```
resource('exchanges')
  .deleted({ id: 1 })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

## $http

* Import the library

```
import { $http } from 'resource'
```

* Initializing

```
const http = $http({
  root: 'localhost:3000/api/v1',
})
```

* Get requiest

```
http
  .get('exchanges', { page_size: 100 })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Post requiest

```
http
  .post('exchanges', { name: 'hello world' })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Put/patch requiest

```
http
  .put('exchanges/1', { id: 1, name: 'hello world' })
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```

* Delete requiest

```
http
  .delete('exchanges/1')
  .then(res => console.log(res.body.data))
  .catch(err => console.error(err))
```
